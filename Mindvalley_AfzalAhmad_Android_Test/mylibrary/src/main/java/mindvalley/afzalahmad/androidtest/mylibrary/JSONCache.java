package mindvalley.afzalahmad.androidtest.mylibrary;

import android.support.v4.util.LruCache;
import org.json.JSONObject;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */

public class JSONCache extends LruCache<String, JSONObject> implements JSONCacheListener {
    private static String TAG = JSONCache.class.getName();
    public JSONCache(int maxSize) {
        super(Utils.getCacheSizeForText(maxSize));
    }

    @Override
    protected int sizeOf(final String key, JSONObject object) {
        return Utils.sizeOf(object);
    }

    @Override
    protected void entryRemoved(boolean evicted, String key, JSONObject oldValue, JSONObject newValue) {
        super.entryRemoved(evicted, key, oldValue, newValue);
    }

    public void clear() {
        evictAll();
    }

    @Override
    public void addJSONToMemoryCache(String url, JSONObject data) {
        if (getJSONFromMemoryCache(url) == null) {
            put(url, data);
        }

    }

    @Override
    public JSONObject getJSONFromMemoryCache(String url) {
        return get(url);
    }
}
