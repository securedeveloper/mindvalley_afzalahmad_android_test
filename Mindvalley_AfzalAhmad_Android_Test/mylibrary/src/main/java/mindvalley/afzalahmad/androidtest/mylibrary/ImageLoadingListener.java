package mindvalley.afzalahmad.androidtest.mylibrary;

import android.graphics.Bitmap;
import android.view.View;

/**
 * Created by AfzalAhmad on 11/17/2016.
 */

public interface ImageLoadingListener {
    public void onLoadingStarted(View view);
    public void onLoadingFailed(View view, ImageLoadingException reason);
    public void onLoadingComplete(View view, CustomDrawable drawable);
    public void onLoadingCancelled(View view);
}
