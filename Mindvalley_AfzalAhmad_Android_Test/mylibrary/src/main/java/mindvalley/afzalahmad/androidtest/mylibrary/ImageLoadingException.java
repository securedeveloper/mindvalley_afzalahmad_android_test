package mindvalley.afzalahmad.androidtest.mylibrary;

/**
 * Created by AfzalAhmad on 11/17/2016.
 */
public class ImageLoadingException extends RuntimeException {
    private static String TAG = ImageLoadingException.class.getName();
    public ImageLoadingException(String message, Throwable cause) {
        super(message,cause);
    }
}