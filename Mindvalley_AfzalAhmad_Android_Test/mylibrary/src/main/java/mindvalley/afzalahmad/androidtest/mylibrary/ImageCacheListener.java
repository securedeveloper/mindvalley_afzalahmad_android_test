package mindvalley.afzalahmad.androidtest.mylibrary;

/**
 * Created by AfzalAhmad on 11/17/2016.
 */

public interface ImageCacheListener {
    public void addImageToMemoryCache(String url, CustomDrawable drawable);
    public CustomDrawable getImageFromMemoryCache(String url);
}
