package mindvalley.afzalahmad.androidtest.mylibrary;

import android.view.View;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */

public interface CustomDownloadListener {
    public void onDownloadStart();
    public void onDownloadProgressChange(Integer progress);
    public void onDownloadCancelled();
    public void onDownloadFailed(DownloadException exception);
    public void onDownloadComplete(String response);
}
