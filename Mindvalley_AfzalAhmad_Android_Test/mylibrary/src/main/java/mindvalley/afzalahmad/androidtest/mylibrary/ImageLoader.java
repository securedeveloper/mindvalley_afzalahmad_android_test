package mindvalley.afzalahmad.androidtest.mylibrary;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by AfzalAhmad on 11/17/2016.
 */

public class ImageLoader implements ImageLoadingListener {

    private static String TAG = ImageLoader.class.getName();
    private static ImageLoader instance = null;
    private Context context;

    ImageLoadingListener imageLoadingListener;

    public synchronized static ImageLoader getInstance() {
        if (instance == null)
        {
            instance = new ImageLoader();
        }
        return instance;
    }

    public ImageLoader init(Context context){
        this.context = context;
        return this;
    }

    public void displayImage(String uRL, ImageView imgView){
        final View view = imgView;
        new AsyncTask<String,Integer,CustomDrawable>(){

            @Override
            protected void onPreExecute() {
                onLoadingStarted(view);
            }

            @Override
            protected void onPostExecute(CustomDrawable drawable) {
                if(drawable != null)
                    onLoadingComplete(view,drawable);
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
            }

            @Override
            protected void onCancelled(CustomDrawable drawable) {
                onLoadingCancelled(view);
            }

            @Override
            protected void onCancelled() {
                onLoadingCancelled(view);
            }

            @Override
            protected CustomDrawable doInBackground(String... urls) {
                CustomDrawable drawable = null;
                try {
                    URL uRL = new URL(urls[0]);
                    URLConnection conn = uRL.openConnection();
                    conn.connect();
                    InputStream is = conn.getInputStream();
                    BufferedInputStream bis = new BufferedInputStream(is);
                    drawable = new CustomDrawable(bis);//BitmapFactory.decodeStream(bis);
                    bis.close();
                    is.close();
                } catch (IOException e) {
                    drawable = null;
                    ImageLoadingException ex = new ImageLoadingException(e.getMessage(),e.getCause());
                    onLoadingFailed(view,ex);
                }
                return drawable;
            }
        }.execute(uRL);
    }

    public void displayImage(final String uRL,final ImageView imgView,final ImageLoadingListener listener){
        final View view = imgView;
        new AsyncTask<String,Integer,CustomDrawable>(){

            @Override
            protected void onPreExecute() {
                listener.onLoadingStarted(view);
            }

            @Override
            protected void onPostExecute(CustomDrawable drawable) {
                if(drawable != null)
                    listener.onLoadingComplete(view,drawable);
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
            }

            @Override
            protected void onCancelled(CustomDrawable drawable) {
                listener.onLoadingCancelled(view);
            }

            @Override
            protected void onCancelled() {
                listener.onLoadingCancelled(view);
            }

            @Override
            protected CustomDrawable doInBackground(String... urls) {
                CustomDrawable drawable = null;
                try {
                    URL uRL = new URL(urls[0]);
                    URLConnection conn = uRL.openConnection();
                    conn.connect();
                    InputStream is = conn.getInputStream();
                    BufferedInputStream bis = new BufferedInputStream(is);
                    drawable = new CustomDrawable(bis);
                    bis.close();
                    is.close();
                } catch (IOException e) {
                    drawable = null;
                    ImageLoadingException ex = new ImageLoadingException(e.getMessage(),e.getCause());
                    listener.onLoadingFailed(view,ex);
                }
                return drawable;
            }
        }.execute(uRL);
    }

    @Override
    public void onLoadingStarted(View view) {

    }

    @Override
    public void onLoadingFailed(View view, ImageLoadingException reason) {

    }

    @Override
    public void onLoadingComplete(View view, CustomDrawable drawable) {
        ImageView imageView = (ImageView)view;
        imageView.setImageDrawable(drawable);
    }

    @Override
    public void onLoadingCancelled(View view) {

    }
}
