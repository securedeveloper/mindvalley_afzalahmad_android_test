package mindvalley.afzalahmad.androidtest.mylibrary;

import android.graphics.Bitmap;
import android.os.Build;
import org.json.JSONObject;
import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * Created by AfzalAhmad on 11/17/2016.
 */

public class Utils {
    private static String TAG = Utils.class.getName();

    public static int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            return data.getByteCount();
        } else {
            return data.getAllocationByteCount();
        }
    }

    public static int getCacheSizeForImage(int maxSize) {
        return (int) (maxSize + 1);
    }

    public static String md5(String txt) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(txt.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            String hashText = bigInt.toString(16);
            while (hashText.length() < 32) {
                hashText = "0" + hashText;
            }
            return hashText;
        } catch (Exception e1) {
            return txt;
        }
    }

    public static int sizeOf(CustomDrawable drawable) {
        return sizeOf(drawable.getBitmap());
    }

    public static int getCacheSizeForText(int maxSize) {
        return (int) (maxSize + 1);
    }

    public static int sizeOf(String text) {
        return text.getBytes().length;
    }

    public static int sizeOf(JSONObject object) {
        return object.toString().getBytes().length;
    }
}
