package mindvalley.afzalahmad.androidtest.mylibrary;


import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */

public class Downloader implements CustomDownloadListener {

    private CustomDownloadListener listener;

    private String data;

    public String getData() {
        return data;
    }

    /*
    Default & Parameterize Constructors
     */
    public Downloader() {
        data = "";
        this.listener = null;
    }

    public Downloader(CustomDownloadListener listener) {
        data = "";
        this.listener = listener;
    }

    public void download(String url){
        try {
            new AsyncTask<String, Integer, String>() {
                @Override
                protected void onPreExecute() {
                    onDownloadStart();
                }

                @Override
                protected void onPostExecute(String response) {
                    onDownloadComplete(response);
                }

                @Override
                protected void onProgressUpdate(Integer... values) {
                    if (values.length > 0)
                        onDownloadProgressChange(values[0]);
                }

                @Override
                protected void onCancelled(String s) {
                    onDownloadCancelled();
                }

                @Override
                protected void onCancelled() {
                    onDownloadCancelled();
                }

                @Override
                protected String doInBackground(String... urls) {
                    String data = null;
                    try {
                        URL url = new URL(urls[0]);
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        InputStream in = urlConnection.getInputStream();
                        InputStreamReader isr = new InputStreamReader(in);
                        BufferedReader br = new BufferedReader(isr);
                        StringBuilder sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line+"\n");
                        }
                        br.close();
                        data = sb.toString();
                    }catch (MalformedURLException e) {
                        onDownloadFailed(new DownloadException(e.getMessage(), e.getCause()));
                    } catch (IOException e) {
                        onDownloadFailed(new DownloadException(e.getMessage(), e.getCause()));
                    } catch (Exception e) {
                        onDownloadFailed(new DownloadException(e.getMessage(), e.getCause()));
                    }
                    return data;
                }
            }.execute(url);
        } catch (Exception ex) {
            onDownloadFailed(new DownloadException(ex.getMessage(), ex.getCause()));
        }
    }

    @Override
    public void onDownloadStart() {
        if(listener != null)
            listener.onDownloadStart();
    }

    @Override
    public void onDownloadProgressChange(Integer progress) {
        if(listener != null)
            listener.onDownloadProgressChange(progress);
    }

    @Override
    public void onDownloadCancelled() {
        if(listener != null)
            listener.onDownloadCancelled();
    }

    @Override
    public void onDownloadFailed(DownloadException exception) {
        if (listener != null)
            listener.onDownloadFailed(exception);
    }

    @Override
    public void onDownloadComplete(String response) {
        if (listener != null)
            listener.onDownloadComplete(response);
        data = response;
    }
}
