package mindvalley.afzalahmad.androidtest.mylibrary;

import org.json.JSONObject;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */

public interface JSONCacheListener {
    public void addJSONToMemoryCache(String url, JSONObject data);
    public JSONObject getJSONFromMemoryCache(String url);
}
