package mindvalley.afzalahmad.androidtest.mylibrary;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static mindvalley.afzalahmad.androidtest.mylibrary.Utils.sizeOf;

/**
 * Created by AfzalAhmad on 11/17/2016.
 */

public class CacheManager implements ImageCacheListener, JSONCacheListener {
    /*
    TAG TO GET CLASS NAME
     */
    private static String TAG = CacheManager.class.getName();
    /*
    We can accommodate Image Cache through single LruCache object but through a HashMap we will be able to control
    memory management and each object of LruCache will be exactly of Image Size
     */
    private HashMap<String, ImageCache> imageMemoryCache;
    /*
    Same thing is with Json Object, We can easily accommodate JSONCache through a single LruCache object
    but due to memory management i am using HashMap to store Cached JSON objects
     */
    private HashMap<String, JSONCache> jSONMemoryCache;

    /*
    Object of CacheManager Class to achieve singleton
     */
    private static CacheManager instance;

    /*
    Context Object
     */
    private static Context context;

    /*
    Constructor
     */
    private CacheManager() {
        imageMemoryCache = new HashMap<String, ImageCache>();
        Log.i(TAG, "imageMemoryCache instance created");
        jSONMemoryCache = new HashMap<String, JSONCache>();
        Log.i(TAG, "jSONMemoryCache instance created");
    }

    /*
    Synchronized Method to get singleton Instance of this class
     */
    public synchronized static CacheManager getInstance() {
        if (instance == null) {
            instance = new CacheManager();
        }
        return instance;
    }

    /*
    Init method to initialize class through a context
     */
    public static CacheManager init(final Context currentContext) {
        context = currentContext;
        return getInstance();
    }


    /*
    Variable to hold memorySize
     */
    private int maxCacheSize = 0;

    public void setMaxCacheSize(int maxCacheSize) {
         if(maxCacheSize == 0 || maxCacheSize > getAvailableCacheMemorySize())
            return;
        Log.i(TAG, "setMaxCacheSize:" + this.maxCacheSize);
        Log.i(TAG, "setMaxCacheSize:" + maxCacheSize);
        this.maxCacheSize = maxCacheSize;
        this.checkCacheMemoryEfficiency(0);
    }

    /*
    Method to get Maximum available Cache Size
     */
    public int getMaxCacheSize() {
        if (maxCacheSize > 0) {
            return maxCacheSize;
        }
        return getAvailableCacheMemorySize();
    }

    /*
    Method to get available Cache Size from device
     */
    public int getAvailableCacheMemorySize() {
        if (context != null) {
            final int memClass = ((ActivityManager) context.getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
            final int cacheSize = 1024 * 1024 * memClass;
            return cacheSize;
        } else {
            return (int) Runtime.getRuntime().maxMemory();
        }
    }

    /*
    Adding Method for Image into Cache
     */
    @Override
    public void addImageToMemoryCache(String url1, CustomDrawable drawable) {
        if(url1 != null || url1.isEmpty() || drawable == null)
            return;
        int size = sizeOf(drawable);
        if(size >= getMaxCacheSize()){
            return;
        }
        //To get rid of invalid characters, i've converted URL to md5 so that we can have a key of limit length for cache
        String url = Utils.md5(url1);
        //before adding object check to see if by adding this object memory will be overflow, if so get rid of least used object
        checkCacheMemoryEfficiency(size);
        //if object is not already added, do add it
        if (getImageFromMemoryCache(url) == null) {
            if (this.imageMemoryCache == null) {
                this.imageMemoryCache = new HashMap<>();
                Log.i(TAG, "imageMemoryCache instance created");
            }
            drawable.setIsCached(true);
            ImageCache cache = new ImageCache(size);
            cache.addImageToMemoryCache(url, drawable);
            imageMemoryCache.put(url, cache);
            cache = null;
            System.gc();
            Log.i(TAG, "image added ," + url1);
        }
    }

    /*
    Getting image back from Cache, if not exist null is returned
     */
    @Override
    public CustomDrawable getImageFromMemoryCache(String url) {
        Log.i(TAG, "image requested," + url);
        url = Utils.md5(url);
        ImageCache entry = imageMemoryCache.get(url);
        Log.i(TAG, "entry returning = " + (entry != null ? "entry" : " null"));
        if (entry != null)
            Log.i(TAG, "image returning = " + (entry.getImageFromMemoryCache(url) != null ? "image" : " null"));
        return entry != null ? entry.getImageFromMemoryCache(url) : null;
    }

    /*
    Method to add JSON data into Cache
     */
    @Override
    public void addJSONToMemoryCache(String url1, JSONObject data) {
        if(url1 != null || url1.isEmpty() || data == null)
            return;
        int size = sizeOf(data);
        if(size >= getMaxCacheSize()){
            return;
        }
        String url = Utils.md5(url1);
        checkCacheMemoryEfficiency(size);
        if (getImageFromMemoryCache(url) == null) {
            if (this.jSONMemoryCache == null) {
                this.jSONMemoryCache = new HashMap<>();
                Log.i(TAG, "jSONMemoryCache instance created");
            }
            JSONCache cache = new JSONCache(size);
            cache.addJSONToMemoryCache(url, data);
            jSONMemoryCache.put(url, cache);
            System.gc();
            Log.i(TAG, "JSON added ," + url1);
        }
    }

    @Override
    public JSONObject getJSONFromMemoryCache(String url) {
        Log.i(TAG, "JSON requested," + url);
        url = Utils.md5(url);
        JSONCache entry = jSONMemoryCache.get(url);
        Log.i(TAG, "entry returning = " + (entry != null ? "entry" : " null"));
        if (entry != null)
            Log.i(TAG, "JSON returning = " + (entry.getJSONFromMemoryCache(url) != null ? "JSON" : " null"));
        return entry != null ? entry.getJSONFromMemoryCache(url) : null;
    }

    /*
    Method to check & Free Memory if needed
     */
    private synchronized void checkCacheMemoryEfficiency(int newSize) {
        Log.i(TAG, "Checking memory efficiency ");
        //Check & See if consumed memory exceeds total allowed memory limit then remove least used objects
        int totalConsumedMemory = newSize;
        int totalAllowedMemory = getMaxCacheSize();
        if(totalAllowedMemory < 1){
            return;
        }
        int imageMemoryLength = imageMemoryCache != null ? imageMemoryCache.size() : 0;
        if (imageMemoryLength > 0) {
            for (Map.Entry<String, ImageCache> imageEntry : imageMemoryCache.entrySet()) {
                totalConsumedMemory += imageEntry.getValue().maxSize();
            }
        }
        int jSONMemoryLength = jSONMemoryCache != null ? jSONMemoryCache.size() : 0;
        if (jSONMemoryLength > 0) {
            for (Map.Entry<String, JSONCache> jSONEntry : jSONMemoryCache.entrySet()) {
                totalConsumedMemory += jSONEntry.getValue().maxSize();
            }
        }
        int memoryLimit = (int)(totalAllowedMemory*0.75F);
        if(totalConsumedMemory >= memoryLimit) {
            Log.i(TAG, "checkCacheMemoryEfficiency: Changing cache memory");
            while (totalConsumedMemory >= memoryLimit) {
                //Removing Least used Image Object from Cache
                int minUse = Integer.MAX_VALUE;
                ImageCache minUsedImageCache = null;
                String url = "";
                for (Map.Entry<String, ImageCache> imageCacheEntry : imageMemoryCache.entrySet()) {
                    ImageCache imageCache = imageCacheEntry.getValue();
                    if(imageCache.hitCount() == 0){
                        minUse = imageCache.hitCount();
                        minUsedImageCache = imageCache;
                        url = imageCacheEntry.getKey();
                        break;
                    }
                    else if (minUse > imageCache.hitCount()) {
                        minUse = imageCache.hitCount();
                        minUsedImageCache = imageCache;
                        url = imageCacheEntry.getKey();
                    }
                    System.gc();
                }
                if(minUsedImageCache != null) {
                    Log.i(TAG, "Releasing " + (minUsedImageCache.maxSize() / LibraryConstants.MB) + "MB");
                    totalConsumedMemory -= minUsedImageCache.maxSize();
                    minUsedImageCache.clear();
                    minUsedImageCache = null;
                    imageMemoryCache.remove(url);
                }
                if (totalConsumedMemory < memoryLimit) {
                    continue;
                }
                System.gc();
                //Removing Least used JSON Object from Cache
                JSONCache minUsedJSONCache = null;
                url = "";
                for (Map.Entry<String, JSONCache> jsonCacheEntry : jSONMemoryCache.entrySet()) {
                    JSONCache jsonCache = jsonCacheEntry.getValue();
                    if (jsonCache.hitCount() == 0) {
                        //In case of Json only remove those elements who have never been used, otherwise keep them stay in cache
                        minUsedJSONCache = jsonCache;
                        url = jsonCacheEntry.getKey();
                        break;
                    }
                }
                if (minUsedJSONCache != null) {
                    Log.i(TAG, "Releasing JSON "+(minUsedJSONCache.maxSize()/LibraryConstants.MB)+"MB");
                    totalConsumedMemory -= minUsedJSONCache.maxSize();
                    minUsedJSONCache.clear();
                    minUsedJSONCache = null;
                    jSONMemoryCache.remove(url);
                }
            }
        }else{
            Log.i(TAG, "checkCacheMemoryEfficiency$Status: newSize = "+(newSize/LibraryConstants.MB)+"MB");
            Log.i(TAG, "checkCacheMemoryEfficiency$Status: Consumed = "+(totalConsumedMemory/LibraryConstants.MB)+"MB");
            Log.i(TAG, "checkCacheMemoryEfficiency$Status: Total = "+(totalAllowedMemory/LibraryConstants.MB)+"MB");
            Log.i(TAG, "checkCacheMemoryEfficiency$Status: Available = "+((totalAllowedMemory-totalConsumedMemory)/LibraryConstants.MB)+"MB");
        }
    }

    /*
    Method to Clear Only JSON data
     */
    public void clearJsonCache() {
        try {
            int jsonMemoryLength = jSONMemoryCache.size();
            if (jsonMemoryLength > 0) {
                JSONCache jsonCache;
                for (Map.Entry<String, JSONCache> jsonCacheEntry : jSONMemoryCache.entrySet()) {
                    jsonCache = jsonCacheEntry.getValue();
                    jsonCache.clear();
                    jsonCache = null;
                }
                jSONMemoryCache.clear();
            }
        } catch (Exception ex) {

        } finally {
            jSONMemoryCache = new HashMap<String, JSONCache>();
        }
    }

    /*
    Method to clear only Image data
     */
    public void clearImageCache() {
        int imageMemoryLength = imageMemoryCache.size();
        if (imageMemoryLength > 0) {
            ImageCache imageCache;
            for (Map.Entry<String, ImageCache> imageCacheEntry : imageMemoryCache.entrySet()) {
                imageCache = imageCacheEntry.getValue();
                imageCache.clear();
                imageCache = null;
            }
            jSONMemoryCache.clear();
        }
    }

    /*
    Method to clear all Cached Data
     */

    public void clear() {
        clearImageCache();
        clearJsonCache();
    }
}
