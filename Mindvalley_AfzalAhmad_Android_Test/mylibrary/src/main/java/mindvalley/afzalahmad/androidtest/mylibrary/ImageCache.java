package mindvalley.afzalahmad.androidtest.mylibrary;

import android.support.v4.util.LruCache;

/**
 * Created by AfzalAhmad on 11/17/2016.
 */

public class ImageCache extends LruCache<String, CustomDrawable> implements ImageCacheListener {

    private static String TAG = ImageCache.class.getName();

    public ImageCache(int maxSize) {
        super(Utils.getCacheSizeForImage(maxSize));
    }

    @Override
    protected int sizeOf(final String key, final CustomDrawable drawable) {
        return Utils.sizeOf(drawable);
    }

    @Override
    protected void entryRemoved(boolean evicted, String key, CustomDrawable oldValue, CustomDrawable newValue) {
        super.entryRemoved(evicted, key, oldValue, newValue);
        oldValue.setIsCached(false);
    }

    public void clear() {
        evictAll();
    }

    @Override
    public void addImageToMemoryCache(String url, CustomDrawable drawable) {
        if (getImageFromMemoryCache(url) == null) {
            drawable.setIsCached(true);
            put(url, drawable);
        }
    }

    @Override
    public CustomDrawable getImageFromMemoryCache(String url) {
        return get(url);
    }
}
