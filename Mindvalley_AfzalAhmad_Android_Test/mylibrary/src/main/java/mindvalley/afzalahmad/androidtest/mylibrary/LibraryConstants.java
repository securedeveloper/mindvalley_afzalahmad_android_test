package mindvalley.afzalahmad.androidtest.mylibrary;

/**
 * Created by AfzalAhmad on 11/17/2016.
 */

public class LibraryConstants {
    public static final Float KB = 1024F;
    private static String TAG = LibraryConstants.class.getName();
    public static String ImageCacheKey="ImageCache";
    public static Float MB = 1048576F;
}
