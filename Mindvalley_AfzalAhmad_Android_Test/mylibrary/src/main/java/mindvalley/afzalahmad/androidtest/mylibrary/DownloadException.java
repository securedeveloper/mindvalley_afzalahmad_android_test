package mindvalley.afzalahmad.androidtest.mylibrary;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */
public class DownloadException extends Exception{
    public DownloadException(String message){
        super(message);
    }

    public DownloadException(String message,Throwable cause){
        super(message, cause);
    }
}
