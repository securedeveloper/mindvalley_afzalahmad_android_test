package mindvalley.afzalahmad.androidtest.model;

import org.json.JSONObject;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */
public class ItemURL implements DataParser{

    /*
    //Start Variable Declaration
     */
    private String raw;
    private String full;
    private String regular;
    private String small;
    private String thumb;

    public String getRaw() {
        return raw;
    }

    public String getFull() {
        return full;
    }

    public String getRegular() {
        return regular;
    }

    public String getSmall() {
        return small;
    }

    public String getThumb() {
        return thumb;
    }
    /*
    //End Variable Declaration
     */

    @Override
    public void parseJSON(JSONObject data) {
        raw = data.optString("raw");
        full = data.optString("full");
        regular = data.optString("regular");
        small = data.optString("small");
        thumb = data.optString("thumb");
    }
}
