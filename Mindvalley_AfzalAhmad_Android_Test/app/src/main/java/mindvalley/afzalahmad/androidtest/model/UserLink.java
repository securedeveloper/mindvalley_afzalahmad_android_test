package mindvalley.afzalahmad.androidtest.model;

import org.json.JSONObject;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */
public class UserLink implements DataParser{

    /*
    //Start Variable Declaration
     */

    private String self;
    private String html;
    private String photos;
    private String likes;

    public String getSelf() {
        return self;
    }

    public String getHtml() {
        return html;
    }

    public String getPhotos() {
        return photos;
    }

    public String getLikes() {
        return likes;
    }

    /*
    //End Variable Declaration
     */

    @Override
    public void parseJSON(JSONObject data) {
        self = data.optString("self");
        html = data.optString("html");
        photos = data.optString("photos");
        likes = data.optString("likes");
    }
}
