package mindvalley.afzalahmad.androidtest;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;

import mindvalley.afzalahmad.androidtest.model.Item;

/**
 * Created by AfzalAhmad on 11/16/2016.
 */

public class ApplicationEngine extends Application {

    public static final String TAG = ApplicationEngine.class.getSimpleName();
    private ArrayList<Item> itemCollect;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public ApplicationEngine() {
        super();
    }

    private Context context;

    private static ApplicationEngine instance;

    public static synchronized ApplicationEngine getInstance() {
        if (instance == null)
            instance = new ApplicationEngine();
        return instance;
    }

    public void init(final Context context){
        this.context = context;
    }

    private RequestQueue requestQueue;

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }

    public ArrayList<Item> getItemCollect() {
        return itemCollect;
    }

    public void setItemCollect(ArrayList<Item> itemCollect) {
        this.itemCollect = itemCollect;
    }
}
