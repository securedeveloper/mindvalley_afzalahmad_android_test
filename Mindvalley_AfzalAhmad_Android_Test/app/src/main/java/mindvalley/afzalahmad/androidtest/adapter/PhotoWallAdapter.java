package mindvalley.afzalahmad.androidtest.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.transition.Fade;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import mindvalley.afzalahmad.androidtest.R;
import mindvalley.afzalahmad.androidtest.model.Item;
import mindvalley.afzalahmad.androidtest.mylibrary.CacheManager;
import mindvalley.afzalahmad.androidtest.mylibrary.CustomDrawable;
import mindvalley.afzalahmad.androidtest.mylibrary.ImageLoader;
import mindvalley.afzalahmad.androidtest.mylibrary.ImageLoadingException;
import mindvalley.afzalahmad.androidtest.mylibrary.ImageLoadingListener;

import static android.transition.TransitionManager.*;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by AfzalAhmad on 11/17/2016.
 */

public class PhotoWallAdapter extends PagerAdapter {

    private Activity activity;
    private ArrayList<Item> items;
    private LayoutInflater inflater;

    public PhotoWallAdapter(final Activity activity, final ArrayList<Item> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Button button;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.fullscreen_image, container, false);
        final ImageView imageView = (ImageView) viewLayout.findViewById(R.id.imgDisplay);
        button = (Button) viewLayout.findViewById(R.id.btnClose);
        final ProgressBar spinner = (ProgressBar) viewLayout.findViewById(R.id.progressBar);
//        spinner.animate();
//        ImageLoader.getInstance().displayImage(items.get(position).getUrls().getFull(), imageView, new ImageLoadingListener() {
//            @Override
//            public void onLoadingStarted(View view) {
//                imageView.setVisibility(View.GONE);
//                spinner.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onLoadingFailed(View view, ImageLoadingException reason) {
//                spinner.setVisibility(View.GONE);
//                imageView.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onLoadingComplete(View view, CustomDrawable drawable) {
//                spinner.setVisibility(View.GONE);
//                imageView.setVisibility(View.VISIBLE);
//                imageView.setImageDrawable(drawable);
//            }
//
//            @Override
//            public void onLoadingCancelled(View view) {
//
//            }
//        });
        loadImage(items.get(position).getUrls().getFull(), spinner, imageView, imageView.getDrawable());
        // close button click event
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });
        ((ViewPager) container).addView(viewLayout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            beginDelayedTransition(container, new Fade(Fade.OUT));
        }
        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }


    public static void loadImage(final String url, final ProgressBar progressBar, final ImageView imageView, final Drawable defaultDrawable) {
        final CustomDrawable cacheDrawable = CacheManager.getInstance().getImageFromMemoryCache(url);
        if (cacheDrawable != null) {
            //Check to see if requested resource is already in cache
            if (progressBar != null) {
                progressBar.setVisibility(GONE);
                imageView.setVisibility(VISIBLE);
            }
            imageView.setImageDrawable(cacheDrawable);
        } else {
            //Resource is not in cache, request for a new one
            ImageLoader.getInstance().displayImage(url, imageView, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(View view) {
                    if (progressBar != null) {
                        imageView.setVisibility(GONE);
                        progressBar.setVisibility(VISIBLE);
                    }
                }

                @Override
                public void onLoadingFailed(View view, ImageLoadingException reason) {
                    if (progressBar != null) {
                        imageView.setVisibility(VISIBLE);
                        progressBar.setVisibility(GONE);
                        imageView.setImageResource(R.drawable.no_image_icon);
                    }
                }

                @Override
                public void onLoadingComplete(View view, CustomDrawable drawable) {
                    if (progressBar != null) {
                        progressBar.setVisibility(GONE);
                        imageView.setVisibility(VISIBLE);
                    }
                    imageView.setImageDrawable(drawable);

                    //Add newly downloaded resource to cache
                    CacheManager.getInstance().addImageToMemoryCache(url, drawable);
                }

                @Override
                public void onLoadingCancelled(View view) {
                    if (progressBar != null) {
                        imageView.setVisibility(VISIBLE);
                        progressBar.setVisibility(GONE);
                        if (defaultDrawable != null)
                            imageView.setImageDrawable(defaultDrawable);
                        else
                            imageView.setImageResource(R.drawable.no_image_icon);
                    }
                }
            });
        }
    }

}
