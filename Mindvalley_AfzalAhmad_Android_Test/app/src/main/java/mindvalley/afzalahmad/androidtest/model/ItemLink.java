package mindvalley.afzalahmad.androidtest.model;

import org.json.JSONObject;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */
public class ItemLink implements DataParser{

    /*
    Variables
     */

    private String self;
    private String html;
    private String download;

    public String getSelf() {
        return self;
    }

    public String getHtml() {
        return html;
    }

    public String getDownload() {
        return download;
    }

    @Override
    public void parseJSON(JSONObject data) {
        self = data.optString("self");
        html = data.optString("html");
        download = data.optString("download");
    }
}
