package mindvalley.afzalahmad.androidtest;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */

public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }
}
