package mindvalley.afzalahmad.androidtest.model;

import android.graphics.Color;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */

public class Item implements DataParser {

    /*
    //START VARIABLE DECLARATION
     */
    private String id;
    private String createdAt;
    private int width;
    private int height;
    private String color;
    private int likes;
    private boolean likedByUser;
    private ItemUser user;
    private ArrayList<UserCollection> currentUserCollection;
    private ItemURL urls;
    private ArrayList<ItemCategory> categories;
    private ItemLink links;

    public String getId() {
        return id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getColor() {
        return color;
    }

    public int getLikes() {
        return likes;
    }

    public boolean isLikedByUser() {
        return likedByUser;
    }

    public ItemUser getUser() {
        return user;
    }

    public ArrayList<UserCollection> getCurrentUserCollection() {
        return currentUserCollection;
    }

    public ItemURL getUrls() {
        return urls;
    }

    public ArrayList<ItemCategory> getCategories() {
        return categories;
    }

    public ItemLink getLinks() {
        return links;
    }

    /*
    //END VARIABLE DECLARATION
     */


    @Override
    public void parseJSON(JSONObject data) {
        id = data.optString("id");
        createdAt = data.optString("created_at");
        width = data.optInt("width");
        height = data.optInt("height");
        color = data.optString("color");
        likes = data.optInt("likes");
        likedByUser = data.optBoolean("liked_by_user");
        user = new ItemUser();
        user.parseJSON(data.optJSONObject("user"));
        currentUserCollection = new ArrayList<>();
        urls = new ItemURL();
        urls.parseJSON(data.optJSONObject("urls"));
        categories = new ArrayList<>();
        JSONArray cats = data.optJSONArray("categories");
        if(cats.length()>0){
            for(int ccount = 0; ccount < cats.length();ccount++){
                ItemCategory category=new ItemCategory();
                try {
                    category.parseJSON(cats.getJSONObject(ccount));
                } catch (JSONException e) {

                }
                categories.add(category);
            }
        }
        links = new ItemLink();
        links.parseJSON(data.optJSONObject("links"));
    }
}
