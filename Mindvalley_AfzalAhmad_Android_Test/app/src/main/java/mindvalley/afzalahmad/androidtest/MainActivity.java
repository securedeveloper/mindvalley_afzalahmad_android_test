package mindvalley.afzalahmad.androidtest;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mindvalley.afzalahmad.androidtest.adapter.SwipeAdapter;
import mindvalley.afzalahmad.androidtest.helper.ConnectionHelper;
import mindvalley.afzalahmad.androidtest.model.Item;
import mindvalley.afzalahmad.androidtest.mylibrary.CacheManager;
import mindvalley.afzalahmad.androidtest.mylibrary.LibraryConstants;
import mindvalley.afzalahmad.androidtest.mylibrary.Utils;

/**
 * Created by AfzalAhmad on 11/17/2016.
 */

public class MainActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    String TAG = MainActivity.class.getSimpleName();
    String dataURL = "";
    SwipeRefreshLayout swipeRefreshLayout;
    ListView listView;
    SwipeAdapter adapter;
    ArrayList<Item> dataList;
    SharedPreferences prefs;
    ConnectionHelper connectionHelper;
    int maxMemoryLimit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ApplicationEngine.getInstance().init(this);
        //Getting Preferences
        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        //Initialize Data & Variables
        dataURL = getString(R.string.data_url);
        connectionHelper = new ConnectionHelper(getApplicationContext());
        dataList = new ArrayList<>();
        listView = (ListView) findViewById(R.id.listView);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(true);
                        fetchData();
                    }
                }
        );
        adapter = new SwipeAdapter(this, dataList);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        fetchData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG,"RESUMED");
        //Getting MaxCacheSize from Preferences
        maxMemoryLimit =  CacheManager.getInstance().getMaxCacheSize();
        int maxSize = prefs.getInt("maxCache", CacheManager.getInstance().getAvailableCacheMemorySize());
        //Synchronizing Maximum Cache Size
        Log.i(TAG, "maxSize:" + String.valueOf(maxSize));
        Log.i(TAG, "maxLimit:" + String.valueOf(maxMemoryLimit));
        if (maxSize != maxMemoryLimit) {

            maxSize *= LibraryConstants.KB;
            CacheManager.getInstance().setMaxCacheSize(maxSize);
        }
    }

    public boolean checkConnectionAvailability() {
        return connectionHelper.checkConnection();
    }

    private void fetchData() {
        if (checkConnectionAvailability()) {
            if (CacheManager.getInstance().getJSONFromMemoryCache(dataURL) != null) {
                if (!LoadDataFromCache()) {
                    LoadDataFromConnection();
                }
            } else {
                LoadDataFromConnection();
            }
        }
    }

    private boolean LoadDataFromCache() {
        try {
            swipeRefreshLayout.setRefreshing(true);
            JSONObject cacheObject = CacheManager.getInstance().getJSONFromMemoryCache(dataURL);
            JSONArray response = cacheObject.getJSONArray(Utils.md5(dataURL));
            if (response.length() > 0) {
                for (int i = 0; i < response.length(); i++) {
                    Item item = new Item();
                    item.parseJSON(response.getJSONObject(i));
                    dataList.add(item);
                }
                if (dataList.size() <= 0) {
                    return false;
                }
                adapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                ApplicationEngine.getInstance().setItemCollect(dataList);
            } else {
                return false;
            }
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    private void LoadDataFromConnection() {
        swipeRefreshLayout.setRefreshing(true);
        JsonArrayRequest req = new JsonArrayRequest(dataURL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        if (response.length() > 0) {
                            try {
                                JSONObject cacheObject = new JSONObject();
                                cacheObject.put(Utils.md5(dataURL), response);
                                CacheManager.getInstance().addJSONToMemoryCache(dataURL, cacheObject);
                                for (int i = 0; i < response.length(); i++) {
                                    Item item = new Item();
                                    item.parseJSON(response.getJSONObject(i));
                                    dataList.add(item);
                                }
                            } catch (JSONException e) {
                                Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                            }
                            Log.i(TAG, dataList.size() + " entries");
                            adapter.notifyDataSetChanged();
                        }
                        swipeRefreshLayout.setRefreshing(false);
                        ApplicationEngine.getInstance().setItemCollect(dataList);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Server Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                // stopping swipe refresh
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        ApplicationEngine.getInstance().addToRequestQueue(req);
    }
}
