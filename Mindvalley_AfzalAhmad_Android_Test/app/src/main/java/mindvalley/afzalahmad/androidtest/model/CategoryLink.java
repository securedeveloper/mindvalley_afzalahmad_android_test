package mindvalley.afzalahmad.androidtest.model;

import org.json.JSONObject;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */
public class CategoryLink implements DataParser{

    /*
    Variables
     */

    private String self;
    private String photos;

    public String getSelf() {
        return self;
    }

    public String getPhotos() {
        return photos;
    }

    @Override
    public void parseJSON(JSONObject data) {
        self = data.optString("self");
        photos = data.optString("photos");
    }
}
