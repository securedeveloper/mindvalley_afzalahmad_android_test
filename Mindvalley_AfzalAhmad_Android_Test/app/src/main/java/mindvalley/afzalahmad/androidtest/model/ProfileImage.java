package mindvalley.afzalahmad.androidtest.model;

import org.json.JSONObject;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */
public class ProfileImage implements DataParser{

    /*
    //Start Variable Declaration
     */
    private String small;
    private String medium;
    private String large;

    public String getSmall() {
        return small;
    }

    public String getMedium() {
        return medium;
    }

    public String getLarge() {
        return large;
    }

    /*
    //End Variable Declaration
     */

    @Override
    public void parseJSON(JSONObject data) {
        small = data.optString("small");
        medium = data.optString("medium");
        large = data.optString("large");
    }
}
