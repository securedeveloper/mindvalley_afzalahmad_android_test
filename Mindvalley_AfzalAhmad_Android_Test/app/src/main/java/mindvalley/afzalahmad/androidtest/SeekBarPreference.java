package mindvalley.afzalahmad.androidtest;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.DecimalFormat;

import mindvalley.afzalahmad.androidtest.mylibrary.CacheManager;
import mindvalley.afzalahmad.androidtest.mylibrary.LibraryConstants;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */

public class SeekBarPreference extends Preference implements SeekBar.OnSeekBarChangeListener {
    private SeekBar seekBar;
    private TextView textView;
    private int progress;
    private static String TAG = "SeekBarPreference";

    public SeekBarPreference(Context context) {
        this(context, null, 0);
    }

    public SeekBarPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SeekBarPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setLayoutResource(R.layout.preference_seekbar);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        seekBar = (SeekBar) view.findViewById(R.id.pref_seekbar);
        textView = (TextView)view.findViewById(R.id.pref_value);
        seekBar.setMax(getMaxAvailableCache());
        if(progress < 1){
            progress = getCurrentMaxCache();
        }
        seekBar.setProgress(progress);
        seekBar.setOnSeekBarChangeListener(this);
        setLabelValue();
    }

    private void setLabelValue() {
        double val = progress/LibraryConstants.KB;
        DecimalFormat formater = new DecimalFormat("#.00");
        textView.setText(formater.format(val)+" MB");
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (!fromUser)
            return;
        setValue(progress);
        setLabelValue();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // not used
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // not used
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        //(Integer) defaultValue
        int val = restoreValue ? getPersistedInt(progress) : getCurrentMaxCache();
        Log.i(TAG,"InitCall, "+(restoreValue?"restore":"default value"));
        Log.i(TAG,String.valueOf(val));
        setValue(val);
    }

    public void setValue(int value) {
        if (shouldPersist()) {
            int val = (int)(value);
            persistInt(val);
        }
        if (value != progress) {
            progress = value;
            notifyChanged();
        }
    }

    public int getMaxAvailableCache(){
        return (int)(CacheManager.getInstance().getAvailableCacheMemorySize()/LibraryConstants.KB);
    }

    public int getCurrentMaxCache(){
        return (int)(CacheManager.getInstance().getMaxCacheSize()/LibraryConstants.KB);
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInt(index, 0);
    }
}
