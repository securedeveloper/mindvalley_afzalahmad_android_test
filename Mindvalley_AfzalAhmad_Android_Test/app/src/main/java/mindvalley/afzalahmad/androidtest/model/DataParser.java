package mindvalley.afzalahmad.androidtest.model;

import org.json.JSONObject;
import org.w3c.dom.Document;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */

public interface DataParser {
    public void parseJSON(JSONObject data);
    //public void parseXML(Document data);
}
