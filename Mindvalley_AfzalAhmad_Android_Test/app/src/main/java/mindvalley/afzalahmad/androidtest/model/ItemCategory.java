package mindvalley.afzalahmad.androidtest.model;

import org.json.JSONObject;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */
public class ItemCategory implements DataParser{

    /*
    //Start Variable Declaration
     */
    private int id;
    private String title;
    private int photoCount;
    private CategoryLink links;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getPhotoCount() {
        return photoCount;
    }

    public CategoryLink getLinks() {
        return links;
    }

    /*
    //End Variable Declaration
     */


    @Override
    public void parseJSON(JSONObject data) {
        id = data.optInt("id");
        title = data.optString("title");
        photoCount = data.optInt("photo_count");
        links = new CategoryLink();
        links.parseJSON(data.optJSONObject("links"));
    }
}
