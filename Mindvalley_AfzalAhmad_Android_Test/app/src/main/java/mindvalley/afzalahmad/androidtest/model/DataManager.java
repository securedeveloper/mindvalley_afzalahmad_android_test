package mindvalley.afzalahmad.androidtest.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Observable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */

public class DataManager extends Observable {
    /*
    Variables
     */
    private ArrayList<Item> items;

    public ArrayList<Item> getItems() {
        return items;
    }

    public DataManager(){
        items = new ArrayList<>();
    }

    public Item getItem(int index) throws IndexOutOfBoundsException,NullPointerException{
        return items.get(index);
    }

    public void parseJSONData(String data) throws JSONException {
        JSONArray arr = new JSONArray(data);
        int dataSize = arr.length();
        if (dataSize > 0) {
            for (int i = 0; i < dataSize; i++) {
                Item item = new Item();
                item.parseJSON(arr.getJSONObject(i));
                items.add(item);
            }
        }
        setChanged();
        notifyAll();
    }

    public void parseXMLData(String data) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(data));
        Document doc = db.parse(is);
        //NodeList nList = doc.getElementsByTagName();
        //TODO: If there is XML parsing
        //
        setChanged();
        notifyAll();
    }

    public ArrayList<Item> parseJSONArray(JSONArray arr) throws JSONException {
        int dataSize = arr.length();
        if (dataSize > 0) {
            for (int i = 0; i < dataSize; i++) {
                Item item = new Item();
                item.parseJSON(arr.getJSONObject(i));
                items.add(item);
            }
        }
        return items;
    }
}
