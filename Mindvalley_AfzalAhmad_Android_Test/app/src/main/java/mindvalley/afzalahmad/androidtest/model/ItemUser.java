package mindvalley.afzalahmad.androidtest.model;

import org.json.JSONObject;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */
public class ItemUser implements DataParser {

    /*
    //START VARIABLE DECLARATION
     */
    private String id;
    private String userName;
    private String name;
    private ProfileImage profileImage;
    private UserLink links;

    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getName() {
        return name;
    }

    public ProfileImage getProfileImage() {
        return profileImage;
    }

    public UserLink getLinks() {
        return links;
    }

    /*
    //END VARIABLE DECLARATION
     */

    @Override
    public void parseJSON(JSONObject data) {
        id = data.optString("id");
        userName = data.optString("username");
        name = data.optString("name");
        profileImage = new ProfileImage();
        profileImage.parseJSON(data.optJSONObject("profile_image"));
        links = new UserLink();
        links.parseJSON(data.optJSONObject("links"));
    }
}
