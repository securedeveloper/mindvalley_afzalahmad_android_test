package mindvalley.afzalahmad.androidtest.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import mindvalley.afzalahmad.androidtest.PhotoWallActivity;
import mindvalley.afzalahmad.androidtest.R;
import mindvalley.afzalahmad.androidtest.helper.ConnectionHelper;
import mindvalley.afzalahmad.androidtest.model.Item;
import mindvalley.afzalahmad.androidtest.mylibrary.CacheManager;
import mindvalley.afzalahmad.androidtest.mylibrary.CustomDrawable;
import mindvalley.afzalahmad.androidtest.mylibrary.ImageLoader;
import mindvalley.afzalahmad.androidtest.mylibrary.ImageLoadingException;
import mindvalley.afzalahmad.androidtest.mylibrary.ImageLoadingListener;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by AfzalAhmad on 11/18/2016.
 */

public class SwipeAdapter extends BaseAdapter {

    private static String TAG = SwipeAdapter.class.getSimpleName();

    Activity activity;
    LayoutInflater inflater;
    List<Item> listData;
    public SwipeAdapter(Activity activity, List<Item> listData) {
        this.activity = activity;
        this.listData = listData;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.listData.size();
    }

    @Override
    public Object getItem(int position) {
        return this.listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            view = inflater.inflate(R.layout.list_row, parent, false);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.list_item_progress_bar);
        final ImageView imageView = (ImageView) view.findViewById(R.id.list_item_thumbnail);
        final Drawable defaultDrawable = imageView.getDrawable();
        final TextView nameTextView = (TextView) view.findViewById(R.id.list_item_name);
        final ImageView avatarImageView = (ImageView) view.findViewById(R.id.list_item_avatar);
        final TextView likesTextView = (TextView) view.findViewById(R.id.list_item_likes);
        final TextView dateTextView = (TextView) view.findViewById(R.id.list_item_date);
        Item item = listData.get(position);
        nameTextView.setText(item.getUser().getName());
        loadImage(item.getUrls().getThumb(), progressBar, imageView, defaultDrawable);
        loadImage(item.getUser().getProfileImage().getSmall(), null, avatarImageView, null);
        setDate(item.getCreatedAt(),dateTextView);
        showCounter(item.getLikes(),likesTextView);
        final int pos = position;
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPhotoWallActivity(pos);
            }
        });
        return view;
    }

    public void openPhotoWallActivity(final int pos){
        Intent intent = new Intent(activity, PhotoWallActivity.class);
        intent.putExtra("position", pos);
        activity.startActivity(intent);
    }

    public static void setDate(final String text,final TextView textView){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            SimpleDateFormat output = new SimpleDateFormat("EEE, d MMM yyyy");
            Date date = sdf.parse(text);
            String formattedDate = output.format(date);
            textView.setText(formattedDate);
        } catch (ParseException e) {
            Log.i(TAG,"SetDateException: "+e.getMessage());
            textView.setText("----");
        }
    }

    public static void showCounter(final int total, final TextView textView) {
        new Thread(new Runnable() {
            int counter = 0;
            public void run() {
                while (counter < total) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    textView.post(new Runnable() {

                        public void run() {
                            textView.setText(Html.fromHtml(counter+" &#128077;"));
                        }
                    });
                    counter++;
                }
            }

        }).start();
    }

    public static void loadImage(final String url, final ProgressBar progressBar, final ImageView imageView, final Drawable defaultDrawable) {
        final CustomDrawable cacheDrawable = CacheManager.getInstance().getImageFromMemoryCache(url);
        if(cacheDrawable != null) {
            //Check to see if requested resource is already in cache
            if (progressBar != null) {
                progressBar.setVisibility(GONE);
                imageView.setVisibility(VISIBLE);
            }
            imageView.setImageDrawable(cacheDrawable);
        }else {
            //Resource is not in cache, request for a new one
            ImageLoader.getInstance().displayImage(url, imageView, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(View view) {
                    if (progressBar != null) {
                        imageView.setVisibility(GONE);
                        progressBar.setVisibility(VISIBLE);
                    }
                }

                @Override
                public void onLoadingFailed(View view, ImageLoadingException reason) {
                    if (progressBar != null) {
                        imageView.setVisibility(VISIBLE);
                        progressBar.setVisibility(GONE);
                        imageView.setImageResource(R.drawable.no_image_icon);
                    }
                }

                @Override
                public void onLoadingComplete(View view, CustomDrawable drawable) {
                    if (progressBar != null) {
                        progressBar.setVisibility(GONE);
                        imageView.setVisibility(VISIBLE);
                    }
                    imageView.setImageDrawable(drawable);

                    //Add newly downloaded resource to cache
                    CacheManager.getInstance().addImageToMemoryCache(url,drawable);
                }

                @Override
                public void onLoadingCancelled(View view) {
                    if (progressBar != null) {
                        imageView.setVisibility(VISIBLE);
                        progressBar.setVisibility(GONE);
                        if (defaultDrawable != null)
                            imageView.setImageDrawable(defaultDrawable);
                        else
                            imageView.setImageResource(R.drawable.no_image_icon);
                    }
                }
            });
        }
    }
}
