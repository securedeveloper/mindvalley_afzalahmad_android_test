package mindvalley.afzalahmad.androidtest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;

import mindvalley.afzalahmad.androidtest.adapter.PhotoWallAdapter;
import mindvalley.afzalahmad.androidtest.model.Item;

/**
 * Created by AfzalAhmad on 11/17/2016.
 */

public class PhotoWallActivity extends Activity {

    private ArrayList<Item> items;
    private PhotoWallAdapter adapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_screen);
        viewPager = (ViewPager) findViewById(R.id.pager);
        items = new ArrayList<>();
        items = ApplicationEngine.getInstance().getItemCollect();
        Intent i = getIntent();
        int position = i.getIntExtra("position", 0);
        adapter = new PhotoWallAdapter(PhotoWallActivity.this,items);

        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(position);
    }
}