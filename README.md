# Mindvalley_AfzalAhmad_Android_Test

This README would normally document whatever steps are necessary to get your application up and running.


## What is this repository for? ##

This repository is my android test for Mindvalley application.

## How do I get set up? ##

This is An Android Studio Project

#### DefaultConfig ####
  * minSdkVersion = 14 
  * targetSdkVersion = 24 
  * Package 'mindvalley.afzalahmad.androidtest'

#### Dependencies ####

<code>
<em>
  { 
   compile fileTree(include: ['.jar'], dir: 'libs') 
   androidTestCompile('com.android.support.test.espresso:espresso-core:2.2.2', {exclude group: 'com.android.support', module: 'support-annotations' }) 
   compile project(':mylibrary') 
   compile 'com.android.support:support-v4:24.2.1' 
   compile 'com.android.support:support-vector-drawable:24.2.1' 
   compile 'com.mcxiaoke.volley:library-aar:1.0.0' 
   testCompile 'junit:junit:4.12' 
   compile 'com.android.support.constraint:constraint-layout:1.0.0-beta4' 
  } 
</em>
</code>
* mylibrary is my custom made library with package of 'mindvalley.afzalahmad.androidtest.mylibrary'

#### Database configuration ####
  * SharedPreferences 
  * LruCache 
  * No database has been used in this application 

## How to run tests ##
  * images & JSON cache
  * Image Loading
  * Multiple Image Loading
  * PhotoWall Testing by clicking on List Item
  * Same Image Loading at multiple places
  * Image Loading Events (onLoadingStarted,onLoadingFailed,onLoadingComplete,onLoadingCancelled)
  * Pull to refresh
  * Cache Memory Management through Setting Menu
  * Calling Cached Objects
  
